package edu.missouri.banks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.canova.api.io.data.Text;
import org.canova.api.records.reader.RecordReader;
import org.canova.api.records.reader.impl.CSVRecordReader;
import org.canova.api.split.FileSplit;
import org.canova.api.writable.Writable;
import org.deeplearning4j.datasets.canova.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Network {
	
	private static final Logger log = LoggerFactory.getLogger(Network.class);	
	
	DataSetIterator data = null;
	MultiLayerConfiguration networkConfig = null;
	INDArray params = null;
	
	String arch = "arch";
	String file = "file";
	
	public Network(){
		
	}
	
	public Evaluation test(){
		
		String[] path = file.split("/");
		String filename = "eval-" + arch + "-" + path[path.length-1] + "";
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(filename, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MultiLayerNetwork network = new MultiLayerNetwork(networkConfig);
		network.init();
		
		int outputNum = 2;
		
		network.setListeners(new ScoreIterationListener(1));
        log.info("Evaluate model....");
        Evaluation eval = new Evaluation(outputNum);
        while(data.hasNext()){
            DataSet ds = data.next();
            INDArray output = network.output(ds.getFeatureMatrix());
            eval.eval(ds.getLabels(), output);
            writer.println(output.toString());
        }
        log.info(eval.stats());
        data.reset();
        
        writer.close();
        log.info("****************Model evaluation finished********************");
        return eval;
	}
	
	public void train(int nEpochs){
		MultiLayerNetwork network = new MultiLayerNetwork(this.networkConfig);
		network.init();
		
		log.info("Train model....");
        network.setListeners(new ScoreIterationListener(1));
        for( int i=0; i<nEpochs; i++ ) {
            network.fit(data.next());
            log.info("*** Completed epoch {} ***", i);
        }
        log.info("****************Model train finished********************");
        
        params = network.params();
	}
	
	
	public void saveConfig(String file){
		System.out.println("Saving " + file);
		try{
			FileUtils.write(new File(file), networkConfig.toJson());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Network loadConfig(String file){
		try {
			networkConfig = MultiLayerConfiguration.fromJson(FileUtils.readFileToString(new File(file)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public void saveParams(String file){
		try{
			DataOutputStream dos = new DataOutputStream(Files.newOutputStream(Paths.get(file)));
	        Nd4j.write(params, dos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Network loadParams(String file){
		try(DataInputStream dis = new DataInputStream(new FileInputStream(file))){
	        params = Nd4j.read(dis);
		} catch(Exception e){
			e.printStackTrace();
		}
		return this;
	}
	
	public Network loadDataFormatted(String file, int batchSize) throws IOException, InterruptedException{
			int numLinesToSkip = 0;
	        String delimiter = ",";
	        RecordReader recordReader = new CSVRecordReader(numLinesToSkip,delimiter);
	        recordReader.initialize(new FileSplit(new File(file)));

	        //Second: the RecordReaderDataSetIterator handles conversion to DataSet objects, ready for use in neural network
	        int labelIndex = 0;     //5 values in each row of the iris.txt CSV: 4 input features followed by an integer label (class) index. Labels are the 5th value (index 4) in each row
	        int numClasses = 2;     //3 classes (types of iris flowers) in the iris data set. Classes have integer values 0, 1 or 2
	        
	        DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader,batchSize,labelIndex,numClasses);
	        data = iterator;
	        return this;
	}
	
	public Network loadDataUnformatted(String file, int batchSize) throws IOException, InterruptedException{
		this.file = file;
		int numLinesToSkip = 0;
		final int numPoints = 85;
        String delimiter = ",";
        RecordReader recordReader = new CSVRecordReader(numLinesToSkip,delimiter){
			@Override
			   public Collection<Writable> next() {
			       Text t = (Text) super.next().iterator().next();
			       while(t.charAt(0) == '#' && hasNext()){
			       	t =  (Text) super.next().iterator().next();
			       }
			       String val = removeFormatting(t.toString());
			       String[] split = val.split(" ");
			       List<Writable> ret = new ArrayList<>();
			       int i = 0;
			       for(String s : split){
			    	   if(i++ < numPoints){
				           ret.add(new Text(s));
			    	   }
			       }
			       return ret;
			   }
			};
        
        
        recordReader.initialize(new FileSplit(new File(file)));

        //Second: the RecordReaderDataSetIterator handles conversion to DataSet objects, ready for use in neural network
        int labelIndex = 0;     //5 values in each row of the iris.txt CSV: 4 input features followed by an integer label (class) index. Labels are the 5th value (index 4) in each row
        int numClasses = 2;     //3 classes (types of iris flowers) in the iris data set. Classes have integer values 0, 1 or 2
        
        DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader,batchSize,labelIndex,numClasses);
        data = iterator;
        return this;
	}
	
	private static String removeFormatting(String val){
		StringBuilder ret = new StringBuilder();
		String[] parts = val.split(" ");
		for(String part : parts){
			String[] vectors = part.split(":");
			if(vectors.length==2)
				ret.append(vectors[1] + " ");
			else if(vectors.length==1){
				if(vectors[0].equals("-1"))
					vectors[0] = "0";
				else if(vectors[0].equals("+1"))
					vectors[0] = "1";
				ret.append(vectors[0] + " ");
			}
			else{
				log.error("Error encountered reading the input file, read empty string, should never occur");
			}
		}
		//log.info(ret.toString());
		return ret.toString();
	}
	
	public Network loadDNFoldConfig(int model, int iter){
		String arch = "dnFoldModel";
		
		switch(model){
			case 1:
				arch = "D84-D100-D100-D30-O1";
				break;
			case 2:
				arch = "D84-D250-D35-O1";
				break;
			case 3:
				arch = "D84-D100-D100-D25-O1";
				break;
			case 4:
				arch = "D84-D100-D100-D25-O1";
				break;
			case 5:
				arch = "D84-D150-D150-D25-O1";
				break;
			case 6:
				arch = "D84-D120-D120-D30-O1";
				break;
			case 7:
				arch = "D84-D250-D35-O1";
				break;
			case 8:
				arch = "D84-D100-D100-D35-O1";
				break;
			case 9:
				arch = "D84-D100-D35-O1";
				break;
			case 10:
				arch = "D84-D100-D100-D30-O1";
				break;
			case 11:
				arch = "D84-D100-D100-D35-O1";
				break;
			case 12:
				arch = "D84-D250-O1";
				break;
			case 13:
				arch = "D84-D150-D150-D25-O1";
				break;
			case 14:
				arch = "D84-D150-D150-D35-O1";
				break;
			default:
				arch = "C84K2S2-D84-D100-D100-D30-O1";
		}
		networkConfig = loadArchConf(arch, iter, .02f, .08f);
		Network n = new Network();
		n.networkConfig = networkConfig;
		n.saveConfig(arch + ".json");
		
		return this;
	}
	
	public Network loadCNNFoldConfig(int model, int iter){
		String arch = "cnnFoldModel";
		
		switch(model){
			case 1:
				arch = "C21K2S1-D100-D30-O1";
				break;
			case 2:
				arch = "C21K4S1-D100-D30-O1";
				break;
			case 3:
				arch = "C42K8S1-D100-D30-O1";
				break;
			case 4:
				arch = "C42K2S2-D150-D35-O1";
				break;
			case 5:
				arch = "C63K4S2-D150-D35-O1";
				break;
			case 6:
				arch = "C63K8S2-D150-D35-O1";
				break;
			case 7:
				arch = "C84K4S2-D150-D25-O1";
				break;
			case 8:
				arch = "C84K8S2-D150-D25-O1";
				break;
			case 9:
				arch = "C105K16S2-D150-D25-O1";
				break;
			case 10:
				arch = "C105K2S2-D150-D25-O1";
				break;
			default:
				arch = "C84K2S2-D84-D100-D100-D30-O1";
		}
		networkConfig = loadArchConf(arch, iter, .02f, .08f);
		Network n = new Network();
		n.networkConfig = networkConfig;
		n.saveConfig(arch + ".json");
		
		return this;
	}
	public Network loadArch(String arch, int iter){
		return loadArch(arch, iter, .02f, .08f);
	}
	public Network loadArch(String arch, int iter, float learning, float momentum){
		loadArchConf(arch, iter, learning, momentum);
		return this;
	}
	
	private MultiLayerConfiguration loadArchConf(String arch, int iter, float learning, float momentum){
		this.arch = arch;
		MultiLayerConfiguration network = null;
		try {
			int[][] ne = DNNGenerator.readInputString(arch);
			network = DNNGenerator.generate(iter,learning, momentum, ne);//TODO change to generateOrig to fix
		} catch (Exception e) {
			System.out.println("Unable to read network config: " + e.toString());
		}
		networkConfig = network;
		Network n = new Network();
		n.networkConfig = networkConfig;
		n.saveConfig(arch + ".json");
		return network;
	}

}
