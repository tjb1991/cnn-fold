package edu.missouri.banks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.deeplearning4j.eval.Evaluation;

public class Main {
	
	private static final boolean cmdLineMode = true;
	
	private static final String programName = "cnn-fold.jar";
	private static final int defaultEpochs = 30;
	private static final int defaultBatch = 1000;
	private static final int defaultIter = 60;
	
	private static final String winPath = "D:/User/User/School/Thesis/Data/";
	private static final String linPath = "/media/tyler/DATA/User/User/School/Thesis/Data/";
	private static final String dataPath = linPath;
	public static void main(String[] args){
		if(cmdLineMode)
			run(args);
		else{
			//automatedResults("dnnResults1.txt");
			//automatedCNNResults("CNNResults_r3.txt");
			bestThreeCNN("Top3Results.txt");
			//bestLearningAndMomentum();
		}
	}
	
	public static String automate(String epochs, String iters, String batch, String learn, String moment, String arch){
		System.out.println("************************");
		System.out.println("AUTOMATING FOR " + arch);
		System.out.println("************************");
		Evaluation eval = null;
		String trainData   = "";
		String testData    = "";
		String param  = arch + ".bin";
		StringBuilder sb = new StringBuilder();
		
		for(int i = 1; i <= 10; i++){
			System.out.println("************************");
			System.out.println("USING TRAIN/TEST SET " + i);
			System.out.println("************************");
			trainData = dataPath + "train/trn" + i + ".txt";
			testData = dataPath + "test/test" + i + ".txt";
			run(("-train -e " + epochs + " -i " + iters + " -b " + batch + " -arch " + arch + " -l " + learn + " -m " + moment + " -param " + param + " -data " + trainData + "").split(" "));//Train
			eval = run(("-evaluate -e " + epochs + " -i " + iters + " -b " + batch + " -arch " + arch + " -l " + learn + " -m " + moment + " -param " + param + " -data " + testData + "").split(" "));//Test
			
			sb.append(arch + "_" + epochs + "\n");
			sb.append("output" + i +"\n");
			sb.append(eval.stats()+"\n\n");
		}
		return sb.toString();
	}
	
	public static void bestThreeCNN(String file){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//Model 1
				String epochs = "30";
				String iters  = "30";
				String batch  = "1000";
				String learn  = ".02";
				String moment = ".08";
				String arch   = "C105K16S2-D150-D25-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));

				//Model 2
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C63K4S2-D150-D35-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
				//Model 3
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C84K4S2-D150-D25-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
			writer.close();
	}
	
	public static void automatedCNNResults(String file){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//Model 1
				String epochs = "30";
				String iters  = "30";
				String batch  = "1000";
				String learn  = ".02";
				String moment = ".08";
				String arch   = "C21K2S1-D100-D30-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));

				//Model 2
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C21K4S1-D100-D30-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
				//Model 3
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C42K8S1-D100-D30-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));

				//Model 4
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C42K2S2-D150-D35-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
				//Model 5
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C63K4S2-D150-D35-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));

				//Model 6
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C63K8S2-D150-D35-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
				//Model 7
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C84K4S2-D150-D25-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));

				//Model 8
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C84K8S2-D150-D25-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
				//Model 9
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C105K16S2-D150-D25-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));

				//Model 10
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C105K2S2-D150-D25-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
				//C84K2S2-D84-D100-D100-D30-O1
				//Model 11
				epochs = "30";
				iters  = "30";
				batch  = "1000";
				learn  = ".02";
				moment = ".08";
				arch   = "C84K2S2-D84-D100-D100-D30-O1";
				writer.println(automate(epochs, iters, batch, learn, moment, arch));
				
		writer.close();
	}
	
	public static void automatedResults(String file){
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//Model 1
		String epochs = "30";
		String iters  = "30";
		String batch  = "1000";
		String learn  = ".02";
		String moment = ".08";
		String arch   = "D84-D100-D100-D30-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 2
		epochs = "60";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D250-D35-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 3
		epochs = "60";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D100-D100-D25-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 4
		epochs = "60";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D100-D100-D25-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 5
		epochs = "60";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D150-D150-D25-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 6
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D120-D120-D30-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 7
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D250-D35-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
				

		//Model 8
		epochs = "60";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D100-D100-D35-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));

		//Model 9
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D100-D35-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 10
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D100-D100-D30-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 11
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D100-D100-D35-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 12
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D250-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 13
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D150-D150-D25-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		//Model 14
		epochs = "30";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "D84-D150-D150-D35-O1";
		writer.println(automate(epochs, iters, batch, learn, moment, arch));
		
		
		//System.exit(0);
		//C84K4S1-D81-D100-D30-O1
		epochs = "50";
		iters  = "30";
		batch  = "1000";
		learn  = ".02";
		moment = ".08";
		arch   = "C84K4S1-D81-D100-D30-O1";
		//automate(epochs, iters, batch, learn, moment, arch);
		
		writer.close();
	}
	
	public static void bestLearningAndMomentum(){
		double bestF1 = 0;
		String bestLearn = "";
		String bestMoment = "";
		
		for(int i = 1; i <= 10; i++){
			for(int j = 1; j <= 10; j++){
				//C84K4S1-D81-D100-D30-O1
				Evaluation eval = null;
				String epochs = "30";
				String iters  = "30";
				String batch  = "1000";
				String learn  = new String((.01f * (float)i) + "");
				String moment = new String((.01f * (float)j) + "");
				String arch   = "D84-D100-D100-D30-O1";
				String param  = arch + ".bin";
				String trainData   = dataPath + "train/trn1.txt";
				String testData    = "";
				System.out.println("**************************************");
				System.out.println("Learn: " + learn + " Moment: " + moment);
				System.out.println("**************************************");
				run(("-train -e " + epochs + " -i " + iters + " -b " + batch + " -arch " + arch + " -l " + learn + " -m " + moment + " -param " + param + " -data " + trainData + "").split(" "));//Train
				for(int k = 1; k <= 1; k++){
					testData = dataPath + "test/test" + k + ".txt";
					eval = run(("-evaluate -e " + epochs + " -i " + iters + " -b " + batch + " -arch " + arch + " -l " + learn + " -m " + moment + " -param " + param + " -data " + testData + "").split(" "));//Test
					if(eval.f1()>bestF1){
						bestF1 = eval.f1();
						bestLearn = learn;
						bestMoment = moment;
					}
				}
			}
		}
		System.out.println("Best F1: " + bestF1);
		System.out.println("Best Learn: " + bestLearn);
		System.out.println("Best Moment: " + bestMoment);
	}
		
	public static Evaluation run(String[] args){
		Options options = new Options();
		CommandLine cmd = createOptions(args, options);
		
		if(args.length == 0){
			printHelp(options);
			System.exit(0);
		}
		
		if(cmd.hasOption("train")){
			String noConfigs = "You must specify: \n" +
					" (-config or -dnfold or -cnnfold or -arch) a network architecture to load \n" + 
					" (-param) a place to store the learned network \n" +
					" (-data) data to load and learn";
			if((cmd.hasOption("config") || cmd.hasOption("dnfold") || cmd.hasOption("cnnfold") || cmd.hasOption("arch")) && cmd.hasOption("param") && cmd.hasOption("data")){
				try {
					train(cmd, options);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else{
				error(options, noConfigs);
			}
			return null;
		}
		else if(cmd.hasOption("evaluate")){
			Evaluation eval = null;
			String noConfigs = "You must specify: \n" +
					" (-config or -dnfold or -cnnfold or -arch) a network architecture to load \n" + 
					" (-param) a place to load a learned network \n" +
					" (-data) data to load and evaluate";
			if((cmd.hasOption("config") || cmd.hasOption("dnfold") || cmd.hasOption("cnnfold") || cmd.hasOption("arch")) && cmd.hasOption("param") && cmd.hasOption("data")){
				try {
					eval = test(cmd, options);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else{
				error(options, noConfigs);
			}
			return eval;
		}
		else{
			error(options, "You must specify -train or -evaluate");
			return null;
		}
	}
	
	private static void train(CommandLine cmd, Options options) throws IOException, InterruptedException{
		String config = "";
		boolean conf = false;
		boolean dnfold = false;
		boolean cnnfold = false;
		boolean arch = false;
		if(cmd.hasOption("config")){
			config = cmd.getOptionValue("config");
			conf = true;
		}
		else if(cmd.hasOption("dnfold")){
			config = cmd.getOptionValue("dnfold");
			dnfold = true;
		}
		else if(cmd.hasOption("cnnfold")){
			config = cmd.getOptionValue("cnnfold");
			cnnfold = true;
		}
		else if(cmd.hasOption("arch")){
			config = cmd.getOptionValue("arch");
			arch = true;
		}
		
		String data = cmd.getOptionValue("data");
		String params = cmd.getOptionValue("param");
		int e = defaultEpochs;
		int b = defaultBatch;
		int i = defaultIter;
		
		if(cmd.hasOption("b")){
			String batch = cmd.getOptionValue("b");
			b = Integer.parseInt(batch);
		}
		
		if(cmd.hasOption("e")){
			String epochs = cmd.getOptionValue("e");
			e = Integer.parseInt(epochs);
		}
		
		if(cmd.hasOption("i")){
			String iter = cmd.getOptionValue("i");
			i = Integer.parseInt(iter);
		}
		
		Network network = null;
		if(dnfold){
			int modelNum = Integer.parseInt(config);
			network = new Network().loadDNFoldConfig(modelNum, i).loadDataUnformatted(data, b);
		}
		else if(cnnfold){
			int modelNum = Integer.parseInt(config);
			network = new Network().loadCNNFoldConfig(modelNum, i).loadDataUnformatted(data, b);
		}
		else if(conf){
			network = new Network().loadConfig(config).loadDataUnformatted(data, b);
		}
		else if(arch){
			if(cmd.hasOption("l") && cmd.hasOption("m")){
				float l = Float.parseFloat(cmd.getOptionValue("l"));
				float m = Float.parseFloat(cmd.getOptionValue("m"));
				network = new Network().loadArch(config, i, l, m).loadDataUnformatted(data, b);
			}
			else
				network = new Network().loadArch(config, i).loadDataUnformatted(data, b);
		}
		
		network.train(e);
		network.saveParams(params);
	}
	
	private static Evaluation test(CommandLine cmd, Options options) throws IOException, InterruptedException{
		String config = "";
		boolean conf = false;
		boolean dnfold = false;
		boolean cnnfold = false;
		boolean arch = false;
		if(cmd.hasOption("config")){
			config = cmd.getOptionValue("config");
			conf = true;
		}
		else if(cmd.hasOption("dnfold")){
			config = cmd.getOptionValue("dnfold");
			dnfold = true;
		}
		else if(cmd.hasOption("cnnfold")){
			config = cmd.getOptionValue("cnnfold");
			cnnfold = true;
		}
		else if(cmd.hasOption("arch")){
			config = cmd.getOptionValue("arch");
			arch = true;
		}
		String data = cmd.getOptionValue("data");
		String params = cmd.getOptionValue("param");
		int b = defaultBatch;
		int i = defaultIter;
		
		if(cmd.hasOption("b")){
			String batch = cmd.getOptionValue("b");
			b = Integer.parseInt(batch);
		}
		
		if(cmd.hasOption("i")){
			String iter = cmd.getOptionValue("i");
			i = Integer.parseInt(iter);
		}
		
		Network network = null;
		if(dnfold){
			int modelNum = Integer.parseInt(config);
			network = new Network().loadDNFoldConfig(modelNum, i).loadParams(params).loadDataUnformatted(data, b);
		}
		else if(cnnfold){
			int modelNum = Integer.parseInt(config);
			network = new Network().loadCNNFoldConfig(modelNum, i).loadParams(params).loadDataUnformatted(data, b);
		}
		else if(conf){
			network = new Network().loadConfig(config).loadParams(params).loadDataUnformatted(data, b);
		}
		else if(arch){
			if(cmd.hasOption("l") && cmd.hasOption("m")){
				float l = Float.parseFloat(cmd.getOptionValue("l"));
				float m = Float.parseFloat(cmd.getOptionValue("m"));
				network = new Network().loadArch(config, i, l, m).loadParams(params).loadDataUnformatted(data, b);
			}
			else
				network = new Network().loadArch(config, i).loadParams(params).loadDataUnformatted(data, b);
		}
		Evaluation eval = network.test();
		return eval;
	}
	
	private static void error(Options options, String msg){
		System.out.println(msg);
		printHelp(options);
		System.exit(1);
	}
	
	private static void printHelp(Options options){
		System.out.println();
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(programName, options, true);
		System.out.println();
		System.out.println("examples:");
		System.out.println(" 1a. create a trained network (-param will create a new file):");
		System.out.println("  " + programName + " -train -config conf-5-8-8-1.json -data trn1.txt -param outFileparam-5-8-8-1.bin ");
		System.out.println();
		System.out.println(" 1b. create a trained network using built in model (-param will create a new file):");
		System.out.println("  " + programName + " -train -dnfold 1 -data trn1.txt -param outFileparam-5-8-8-1.bin ");
		System.out.println();
		System.out.println(" 2a. evaluate a trained network (-param will open a file):");
		System.out.println("  " + programName + " -evaluate -config conf-5-8-8-1.json -param inFileparam-5-8-8-1.bin -data test-all.txt");
		System.out.println();
		System.out.println(" 2b. evaluate a trained network using built in model (-param will open a file):");
		System.out.println("  " + programName + " -evaluate -dnfold 1 -param inFileparam-5-8-8-1.bin -data test-all.txt");
		System.out.println();
		

	}
	
	private static CommandLine createOptions(String[] args, Options options){
		
		OptionGroup testTrainOptionGroup = new OptionGroup();
		OptionGroup networkConfigOptionGroup = new OptionGroup();
		testTrainOptionGroup.setRequired(false);
		
		Option train = new Option("train", "train the network");
		Option test = new Option("evaluate", "test the network");
		Option networkConfig = Option.builder("config")
                .hasArg().argName("file")
                .desc( "use given file loading a network configuration" )
                .build();
		
		Option chengNetworkConfig = Option.builder("dnfold")
                .hasArg().argName("model number")
                .desc( "load a model corresponding to the DNFold paper" )
                .build();
		
		Option cnnNetworkConfig = Option.builder("cnnfold")
                .hasArg().argName("model number")
                .desc( "load a model corresponding to the CNNFold paper" )
                .build();
		Option arch = Option.builder("arch")
				.hasArg().argName("architecture")
				.desc( "create a model from the command line, format should be in C30K4S1-D100-D30-O1, For Convolutional layer with 30 filters, Kernel size of 4, and Stride of 1, dense layer size 84... Output layer size 1" )
				.build();
		
		Option networkParameters = Option.builder("param")
                .hasArg().argName("file")
                .desc( "use given file loading or saving a trained network parameters" )
                .build();
		
		Option networkData = Option.builder( "data" )
                .hasArg().argName("file")
                .desc( "use given file to train a network or test a trained network" )
                .build();
		
		Option batchSize  = Option.builder("b")
                .desc( "optional batch size for the dataset [default:" + defaultBatch +"]" )
                .hasArg().argName("n")
                .build();
		
		Option epochs  = Option.builder("e")
                .desc( "optional epochs to run the network [default:" + defaultEpochs +"]" )
                .hasArg().argName("n")
                .build();

		Option iter  = Option.builder("i")
                .desc( "optional iterations to run the network each epoch [default:" + defaultIter +"]" )
                .hasArg().argName("n")
                .build();
		Option learning = Option.builder("l")
				.desc( "optional learning rate, works with arch only" )
				.hasArg().argName("n")
				.build();
		Option momentum = Option.builder("m")
				.desc( "optional momentum, works with arch only" )
				.hasArg().argName("n")
				.build();
		
		testTrainOptionGroup.addOption(train);
		testTrainOptionGroup.addOption(test);
		networkConfigOptionGroup.addOption(networkConfig);
		networkConfigOptionGroup.addOption(chengNetworkConfig);
		networkConfigOptionGroup.addOption(cnnNetworkConfig);
		networkConfigOptionGroup.addOption(arch);
		options.addOption(networkParameters);
		options.addOption(networkData);
		options.addOptionGroup(testTrainOptionGroup);
		options.addOptionGroup(networkConfigOptionGroup);
		options.addOption(batchSize);
		options.addOption(epochs);
		options.addOption(iter);
		options.addOption(learning);
		options.addOption(momentum);
		
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
		
		return cmd;
	}

}
