package edu.missouri.banks;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration.ListBuilder;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.conf.layers.setup.ConvolutionLayerSetup;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;

	public class DNNGenerator {
		
		final static int CONVOLAYER = 0;
		final static int DENSELAYER = 1;
		final static int OUTPUTLAYER = 2;
		final static int SAMPLELAYER = 3;
		
		final static int defaultKernelSize = 4;
		final static int defaultStrideSize = 1;
		
	public static MultiLayerConfiguration generate(int iter, float learningRate, float momentum, int[][] layerConf) throws Exception{
		
		int numLayers = layerConf.length;
		int currentLayer = 0;
		int outputNum = 2;//Ignore any user input, we're doing a binary classification
		boolean convo = false;
		//Tanh for output layer [-1, 1]
		
		ListBuilder builder = new NeuralNetConfiguration.Builder()
				.seed(System.currentTimeMillis())
				.iterations(iter)
				.learningRate(learningRate)
		        .momentum(momentum)
		        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
		        .list(numLayers);
		
				for(int i = 0; i < numLayers; i++){
					if(layerConf[i][0] == CONVOLAYER){
						convo = true;
						builder.layer(currentLayer++, new ConvolutionLayer.Builder(1,layerConf[i][2])
		                        .stride(1,layerConf[i][3])
		                        .nIn(1).nOut(layerConf[i][1])//input is 1 because we have only 1 channel | IS THE OUT THE ACTIVATION MAPS?
		                        .activation("relu")
		                        .build());
					}
					else if(layerConf[i][0] == SAMPLELAYER){//Not currently implemented TODO
						builder.layer(currentLayer++, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
								.kernelSize(1,layerConf[i][2])//This will eliminate the remaining zeros
								.stride(1,layerConf[i][3])
								.build());
					}
					else if(layerConf[i][0] == DENSELAYER){
						int nOut = -1;
						int nIn = -1;
//						nIn = layerConf[i][1];
//						if(layerConf[i+1][0] == OUTPUTLAYER)
//							nOut = layerConf[i][1];
//						else if(i>0 && layerConf[i-1][0] == CONVOLAYER){
//							nIn = layerConf[i-1][1];
//						}
//						else{
//							nOut = layerConf[i+1][1];
//						}
						
						if(i==0){
							nIn = layerConf[i][1];
							nOut = layerConf[i][1];
						}
						else{
							nIn = layerConf[i-1][1];
							nOut = layerConf[i][1];
						}
						
						builder.layer(currentLayer++, new DenseLayer.Builder()
								 .nIn(nIn)
								 .nOut(nOut)
		                         .weightInit(WeightInit.XAVIER)
		                         .activation("sin")
		                         .dropOut(.5f)
		                         .build());
					}
					else if(layerConf[i][0] == OUTPUTLAYER){
						builder.layer(currentLayer++, new OutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD).activation("softmax")
	                         .nIn(layerConf[i-1][1]).nOut(outputNum).build());
					}
					else{
						throw new Exception("Unable to read layer type");
					}
				}
		          
				MultiLayerConfiguration.Builder net = builder.pretrain(false).backprop(true);
		
				if(convo){
					new ConvolutionLayerSetup(net,1,84,1);
				}
				
        MultiLayerConfiguration conf = net.build();
				
		return conf;
	}
	
	public static MultiLayerConfiguration generateOrig(int iter, float learningRate, float momentum, int[][] layerConf) throws Exception{
		
		int numLayers = layerConf.length;
		int currentLayer = 0;
		int outputNum = 2;//Ignore any user input, we're doing a binary classification
		boolean convo = false;
		//Tanh for output layer [-1, 1]
		
		ListBuilder builder = new NeuralNetConfiguration.Builder()
				.seed(System.currentTimeMillis())
				.iterations(iter)
				.learningRate(learningRate)
		        .momentum(momentum)
		        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
		        .list(numLayers);
		
				for(int i = 0; i < numLayers; i++){
					if(layerConf[i][0] == CONVOLAYER){
						convo = true;
						builder.layer(currentLayer++, new ConvolutionLayer.Builder(1,layerConf[i][2])
		                        .stride(1,layerConf[i][3])
		                        .nIn(layerConf[i][1]).nOut(layerConf[i+1][1])
		                        .activation("relu")
		                        .build());
						
					}
					else if(layerConf[i][0] == DENSELAYER){
						int nOut = 83;
						int nIn = layerConf[i][1];
						if(layerConf[i+1][0] == OUTPUTLAYER)
							nOut = layerConf[i][1];
						else if(i>0 && layerConf[i-1][0] == CONVOLAYER){
							nIn = layerConf[i-1][1];
						}
						else{
							nOut = layerConf[i+1][1];
						}
						builder.layer(currentLayer++, new DenseLayer.Builder()
								 .nIn(nIn).nOut(nOut)
		                         .weightInit(WeightInit.XAVIER)
		                         .activation("sin")
		                         .dropOut(.5f)
		                         .build());
					}
					else if(layerConf[i][0] == OUTPUTLAYER){
						builder.layer(currentLayer++, new OutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD).activation("softmax")
	                         .nIn(layerConf[i-1][1]).nOut(outputNum).build());
					}
					else{
						throw new Exception("Unable to read layer type");
					}
				}
		          
				MultiLayerConfiguration.Builder net = builder.pretrain(false).backprop(true);
		
				if(convo){
					new ConvolutionLayerSetup(net,1,84,1);
				}
				
        MultiLayerConfiguration conf = net.build();
				
		return conf;
	}
	
	/**
	 * 
	 * Format should be in C84K4S1-D84-D100-D100-D30-O1
	 * For Convolutional layer of size 84 input, Kernel size of 4, and Stride of 1
	 * For Dense layer of size 84 ...
	 * For Output layer of size 1
	 * 
	 */
	public static int[][] readInputString(String network) throws Exception{
		boolean dense = false;
		String[] net = network.split("-");
		int[][] ret = new int[net.length][4];
		int currentLayer = 0;
		for(String layer:net){
			String[] conf = layer.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
			for(int i = 0; i < conf.length; i++){
				if(conf[i].equals("C")){
					if(dense){
						throw new Exception("You can't have a convolutional layer after a dense layer");
					}
					ret[currentLayer][0] = CONVOLAYER;
					ret[currentLayer][1] = Integer.parseInt(conf[i+1]);
					if(conf[i+2] != null && conf[i+2].equals("K")){
						ret[currentLayer][2] = Integer.parseInt(conf[i+3]);
					}
					else{
						ret[currentLayer][2] = defaultKernelSize;
					}
					if(conf[i+4] != null && conf[i+4].equals("S")){
						ret[currentLayer][3] = Integer.parseInt(conf[i+5]);
					}
					else{
						ret[currentLayer][3] = defaultStrideSize;
					}
					i+=5;
				}
				else if(conf[i].equals("D")){
					dense = true;
					ret[currentLayer][0] = DENSELAYER;
					ret[currentLayer][1] = Integer.parseInt(conf[i+1]);
					i+=1;
				}
				else if(conf[i].equals("O")){
					ret[currentLayer][0] = OUTPUTLAYER;
					ret[currentLayer][1] = Integer.parseInt(conf[i+1]);
					i+=1;
				}
				else{
					throw new Exception("Unrecognized layer type");
				}
			}
			currentLayer++;
		}
		return ret;
	}
	
	public static void print2DArray(int[][] ne){
		for(int i = 0; i < ne.length; i++){
			for(int j = 0; j < ne[i].length; j++){
				System.out.print(ne[i][j] + " ");
			}
			System.out.println();
		}
	}

}
