# Tyler Banks #
# The University of Missouri Masters Project 2016 #

This program is an implementation of a Convolutional Neural Network using DL4J on a 1 dimensional dataset provided by Dr. Cheng. See http://iris.rnet.missouri.edu/dnfold/ for the training and testing sets.

```
#!java

Help:

usage: cnn-fold.jar [-arch <architecture> | -cnnfold <model number> |
       -config <file> | -dnfold <model number>] [-b <n>]   [-data <file>]
       [-e <n>] [-evaluate | -train] [-i <n>] [-l <n>] [-m <n>] [-param
       <file>]
 -arch <architecture>      create a model from the command line, format
                           should be in C30K4S1-D100-D30-O1, For
                           Convolutional layer with 30 filters, Kernel
                           size of 4, and Stride of 1 - dense layer size
                           100 - ... - Output layer size 1
 -b <n>                    optional batch size for the dataset
                           [default:1000]
 -cnnfold <model number>   load a model corresponding to the CNNFold paper
 -config <file>            use given file loading a network configuration
 -data <file>              use given file to train a network or test a
                           trained network
 -dnfold <model number>    load a model corresponding to the DNFold paper
 -e <n>                    optional epochs to run the network [default:30]
 -evaluate                 test the network
 -i <n>                    optional iterations to run the network each
                           epoch [default:60]
 -l <n>                    optional learning rate, works with arch only
 -m <n>                    optional momentum, works with arch only
 -param <file>             use given file loading or saving a trained
                           network parameters
 -train                    train the network

examples:
 1a. create a trained network (-param will create a new file):
  cnn-fold.jar -train -config conf-5-8-8-1.json -data trn1.txt -param outFileparam-5-8-8-1.bin 

 1b. create a trained network using built in model (-param will create a new file):
  cnn-fold.jar -train -dnfold 1 -data trn1.txt -param outFileparam-5-8-8-1.bin 

 2a. evaluate a trained network (-param will open a file):
  cnn-fold.jar -evaluate -config conf-5-8-8-1.json -param inFileparam-5-8-8-1.bin -data test-all.txt

 2b. evaluate a trained network using built in model (-param will open a file):
  cnn-fold.jar -evaluate -dnfold 1 -param inFileparam-5-8-8-1.bin -data test-all.txt

 3a. create a trained network using the arch command
  cnn-fold.jar -train -arch C10K2S2-D100-D34-O1 -param C10Param.bin -data trn1.txt

 3b. json file automatically created using the arch command can be reconfigured and loaded using the config command

```